import pandas as pd
import plotly.express as px  # (version 4.7.0)
import plotly.graph_objects as go
import numpy as np
import os
import dash  # (version 1.12.0) pip install dash
import dash_core_components as dcc
import dash_html_components as html
from dash.dependencies import Input, Output

from django_plotly_dash import DjangoDash
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

app = DjangoDash('moistmap')

# ------------------------------------------------------------------------------
# Import and clean data (importing csv into pandas)

moist_table = pd.DataFrame()
for i in range(20):
    data_path = os.path.join(BASE_DIR, 'maps', 'moist_map_array_'+str(i)+'.txt')
    df = pd.read_csv(data_path, delimiter = ',', header=None)
    moist_table[i] = df.values.flatten()


# ------------------------------------------------------------------------------
# App layout
app.layout = html.Div([

    #html.H1("Moisture dashboard", style={'text-align': 'center'}),
  

    dcc.Graph(id='moist_map', figure={}),
    
    dcc.Slider(id="slct_array",
        min=0,
        max=19,
        value=10,
        step=None,
        dots=True,
        marks={i: 'array{}'.format( i) for i in range(20)           
        },
    ),

    html.Div(id='output_container', children=[]), 
    html.Br(),


])


# ------------------------------------------------------------------------------
# Connect the Plotly graphs with Dash Components
@app.callback(
    [Output(component_id='output_container', component_property='children'), #output 1 - year
     Output(component_id='moist_map', component_property='figure')], #output 2 - figure
    [Input(component_id='slct_array', component_property='value')] #slider

)
def update_graph(option_slctd):

    print(option_slctd)
    print(type(option_slctd))
    container = "The array chosen by user was: {}".format(option_slctd)

    if option_slctd=="mean":
        a=moist_table.mean(axis=1)
    else:
        a=moist_table[option_slctd]

    b=np.matrix(a.tolist())
    c=b.reshape(40,40)


    #Plotly Graph Objects (GO)
    fig = go.Heatmap(
        x= np.arange(40),
        y= np.arange(40),
        z= c,
        type='heatmap',
        colorscale='Viridis'    
    )

    data = [fig]
    fig = go.Figure(data=data)
    #fig.show()

    from plotly.offline import plot
    import sys

    plt_div = plot(fig, output_type='div')

    
    fig.update_layout(
        title_text="Moisture",
        title_xanchor="center",
        title_font=dict(size=24),
        title_x=0.5,
        width = 7000, 
        height = 400,
    )

    return container, fig #output1-year #output2-figure


# ------------------------------------------------------------------------------
